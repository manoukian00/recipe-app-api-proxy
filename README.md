# Recipe app API proxy

Nginx proxy app for recipe app API

## Usage

### Environment variables

 * `LISTEN_PORT` - Port to listen on (default: `8000`) 
 * `APP_HOST` - App host to forward requests to (default: `app`)
 * `APP_PORT` - Port for app to forward requests to (default: `9000`)
